export default [
	{
		name: "Webstoemp",
		url: "https://www.webstoemp.com/",
	},
	{
		name: "Andy Bell",
		url: "https://andy-bell.co.uk/",
	},
];
