export default {
	title: "David JULIEN",
	subtitle: "Le blog d'un doctorant en informatique.",
	url: "https://www.davidjulien.xyz",
	language: "fr",
	description:
		"Le blog d'un doctorant en informatique... Mais on pourrait parler de cuisine, aussi.",
	author: {
		name: "David JULIEN",
		email: "swytch@mailo.com",
		academic: "david.julien@univ-nantes.fr",
	},
	rss: "/feed.xml",
};
