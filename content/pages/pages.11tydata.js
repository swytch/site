export default {
	layout: "layouts/base.njk",
	eleventyComputed: {
		permalink(data) {
			if (data.permalink) {
				return data.permalink;
			}
			const titleSlug = this.slugify(data.title, { lower: true });
			return `/${titleSlug}/`;
		},
	},
};
