---
title: "End-to-End Statistical Model Checking for Parameterization and Stability Analysis of ODE Models"
author: "D.JULIEN, G. ARDOUREL, G.CANTIN, B.DELAHAYE"
journal: "ACM Transactions on Modeling and Computer Simulation"
publication_date: "July 2024"
bib: "tomacs2023.bib"
hal: "hal-04478120"
doi: "10.1145/3649438"
pdf: "tomacs2023.pdf"
---
