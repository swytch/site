export default {
	tags: ["posts"],
	layout: "layouts/post.njk",
	eleventyComputed: {
		permalink(data) {
			if (data.permalink) {
				return data.permalink;
			}
			const { date } = data.page;
			const dateURL = date.toISOString().split("T")[0];
			const titleSlug = this.slugify(data.title, { lower: true });
			return `blog/${dateURL}-${titleSlug}/`;
		},
	},
};
