---
title: "On prend les mêmes, et on recommence."
updated: 2025-02-20
excerpt: "Bon, pas de quoi crier de joie"
---

## Je suis de retour !

Bon, pas de quoi crier de joie.
Y'a quelques temps, on m'a fait remarquer que je n'avais jamais donné suite à
mon article concernant mon Google Pixel.
Voilà : il n'y a rien de spécial à en dire, tout marche bien.
Plus sérieusement, je prends enfin un peu de temps pour faire des choses, autant
en profiter pour remonter ce site.

## Et donc, qu'est-ce-que je vais faire ?

J'aimerais y intégrer une section « Cuisine », déjà ;
on verra à quel point c'est faisable sur le long terme.
Étant végétalien, on me demande parfois ce que l'on peut me préparer pour un
repas, ou si je peux passer le recette de mon brownie ou de ma mayonnaisse
végétale.
Il pourrait **enfin** y avoir une section dédiée vers laquelle rediriger tout le
monde !

À court terme en revanche, je vais profiter de devoir dépoussiérer et mettre à
jour le *framework* qui génère ce site pour faire quelques articles.

À plus !
