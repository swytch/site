---
title: "La gestion des images"
excerpt: "Vous avez dit \"automatiser \" ?"
img:
    src: "./img/solutre.jpg"
    alt: "Un paysage de Bourgogne avec un rocher en arrière plan, au milieu des vignes"
    artist: "le Sixième Rêve"
    year: "2021"
    href: "https://unsplash.com/fr/photos/une-vue-dun-champ-herbeux-avec-des-montagnes-en-arriere-plan-oViFHB4DOsc"
---

## Encore une fois c'est très facile

L'avantage d'utiliser un *framework* pour générer son site, c'est que d'autres
personnes plus qualifiées que soi peuvent proposer des outils pour faire les
chose à notre place (vous avez dit « automatiser » ?).
Et donc quand il s'agit de gérer des images et réduire leur taille, je suis bien
content d'avoir une solution clef-en-main pour le faire à ma place.

Le *plugin* d'aujourd'hui s'appelle `@11ty/eleventy-img`, et il permet deux
choses:

- la compression d'une image pour l'adapter à la (ou les) définition adéquate
  dans la balise HTML `<img>` ou `<picture>` ;
- le choix de la bonne balise (`<img>` ou `<picture>`) en fonction du cas.

Pour le deuxième point, vous pouvez vous référer à la
[documentation](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture)
Mozilla qui indique qu'une balise `<picture>` est utile notamment pour gérer une
image en plusieurs format, et permettre au client de ne télécharger que la
version la plus adaptée à son écran.

Comme d'habitude, ça se passe notamment dans `/eleventy.config.js` :

```js
import { eleventyImageTransformPlugin } from "@11ty/eleventy-img";

export default async function (eleventyConfig) {
    // Watch images for the image pipeline.
    eleventyConfig.addWatchTarget("content/**/*.{svg,webp,png,jpg,jpeg,gif}");

    // Image optimization: https://www.11ty.dev/docs/plugins/image/#eleventy-transform
    eleventyConfig.addPlugin(eleventyImageTransformPlugin, {
        // Output formats for each image.
        formats: ["auto"],

        // Skip SVG if the output is smaller than original
        svgShortCircuit: "size",

        failOnError: false,

        htmlOptions: {
            // e.g. <img loading decoding> assigned on the HTML tag will override these values.
        	imgAttributes: {
        		loading: "lazy",
        		decoding: "async",
        	},
        },

        // To process animated `gif` or `webp` files
        sharpOptions: {
        	animated: true,
        },
    });
}
```

## Et c'est tout ?

Et c'est tout.
On peut maintenant ré-utiliser les autres avantages d'un *framework*, comme les
*templates* et l'en-tête des fichiers source pour spécifier une image de
couverture (comme en haut de cet article).
*Eleventy* se chargera de tout re-dimensionner pour que ça ait la bonne tête, de
mettre les fichiers au bon endroit pour que tout marche bien.

Je pense parfois mettre une image en en-tête de billets de blog.
Pour garder une certaine cohérence, je veux que cette image apparaisse toujours
en haut de l'article (entre la date et le titre), qu'elle mesure 600px de large
par défaut (avec la possibilité de modifier au besoin), qu'elle comporte un
attribut `alt` pour les lecteurs de page web, et créditer l'aritste qui a créé
cette image.

Tout ça peut être spécifié dans l'en-tête du fichier source :
```yaml
{% raw %}---
title: La gestion des images
img:
    src: "./img/solutre.jpg"
    alt: "Un paysage de Bourgogne avec un rocher en arrière plan, au milieu des vignes"
    artist: "le Sixième Rêve"
    year: "2021"
    href: "https://unsplash.com/fr/photos/une-vue-dun-champ-herbeux-avec-des-montagnes-en-arriere-plan-oViFHB4DOsc"
---{% endraw %}
```

Derrière, il suffit de spécifier comment gérer ces infos dans le billet final :
```html
{% raw %}{% if img %}
<figure>
    <img class="c-blogpost__img" src="{{ img.src }}" alt="{{ img.alt }}" width="{{ img.width or 600 }}">
    <figcaption>©
        <a href="{{ img.href }}"> {{ img.artist }} </a> - {{ img.year }}
    </figcaption>
</figure>
{% endif %}{% endraw %}
```

## *Let's call it a day!*

Évidemment, allez faire un tour sur la
[doc](https://www.11ty.dev/docs/plugins/image/) du *plugin* en cas de besoin.

À plus !
