---
title: "« Propulsé par Eleventy. »"
description: "Un post pour expliquer le framework que j'utilise."
excerpt: "Un framework JavaScript pour monter ton site ?"
tags:
  - "framework"
  - "web"
links:
  - "https://jekyllrb.com/"
  - "https://www.11ty.dev/"
  - "https://www.11ty.dev/docs/starter/"
---

## Il faut bien commencer quelque part

Monter un site, ça peut vite devenir compliqué.
L'apport du contenu est presque la partie facile: il faut également penser à la
structure du site, la présentation des différents types de posts, la
navigation...

N'étant pas développeur web, je me suis tourné vers les différents
*frameworks* proposés par la communauté pour répondre à mes besoins.
Je voulais deux choses:
- un site **statique**, simple, rapide et (idéalement) lisible dans un terminal
- une génération automatique de la structure, avec gestion des liens internes

La solution la plus populaire est probablement Jekyll[0], développé par
l'équipe derrière GitHub.
Malheureusement, l'environnement de dev Ruby n'est pas facile à mettre en
place, surtout sur Gentoo.

## Un...opossum?

Eleventy[1] est un autre utilitaire de génération de site statique.
Également opensource, il est écrit en NodeJS et profite donc de la (relative)
simplicité de développement de JavaScript, et notamment une gestion simple des
dépendances via `npm` et le `package.json`.

Le seul inconvénient, à priori, est que Eleventy est moins propre à la sortie
du carton: il y a des configs à mettre en place, et moins de *templates* tous
faits, prêts à être utilisés.

Cela dit, une fois qu'on a compris comment ça marchait (la doc est bien faite,
et il y a des *starters*[2] dont on peut s'inspirer, ou que l'on peut carrément
*forker*), le *framework* se trouve agréable à utiliser.
Il y a peu de fioritures, les fonctionnalités basiques nous empêchent de faire
des choses compliquées, mais tout est là : *templating*, récupération de
données, injection de données à partir de fichier JSON, formattage des dates,
des liens...

Bref, je suis content.
Hâte de voir comment ça évolue, mais je suis confiant: après une v1.0.0 en
2022, l'équipe a sorti une v2.0.0 fin janvier 2023!
