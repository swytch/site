---
title: "Hello, world!"
description: "Post d'introduction, pour signifier l'arrivée du site"
excerpt: "..."
tags:
    - "meta"
links:
    - "https://neovim.io"
    - "https://arn-fai.net"
    - "https://wiki.arn-fai.net/benevoles:technique:matos"
    - "https://ovhcloud.com"
---

## J'ai un peu hésité à monter ce site.

D'un côté, j'avais envie d'avoir un espace d'expression sur le net.
Un espace où je pourrais, notamment, proposer du contenu éducatif sur Linux et
les outils alternatifs aux ténors du numérique, Google et Microsoft en premier
lieu.

De l'autre, j'avais peur de ne pas avoir grand chose à dire après 3 tutos et
quelques *posts*.

## Le grand saut

Et je me suis quand même lancé.
Je me suis dit que le pire qui pouvais m'arriver c'est d'avoir créé ce site
« pour rien », même si j'aurais quand même appris quelques trucs au passage
(comment paramétrer un serveur, avoir une gestion saine du contenu...).

## Les outils

### L'OS

Linux, bien sûr!

### Le terminal

Je suis un fervent avocat de l'utilisation du terminal (« invite de commande »
sous Windows).
Le terminal ne cache rien, il est simple, rapide, efficace.
Tout est texte, et le texte c'est bien : c'est accessible, transférable, léger.
Vous avez déjà essayé de cliquer sur un bouton **automatiquement** ?
En dehors d'une page HTML, c'est franchement pas simple.
Les applications en ligne de commande (*CLI* en anglais) ne souffrent d'aucune
limitation de ce genre : on peut envoyer la sortie de la première dans l'entrée
de la deuxième, et donc créer une chaîne de modification d'une entité de texte,
simplement.
Le comportement des applications CLI est généralement explicite : pour réaliser
une action particulière, on utilise des *flags* qui disent clairement ce que
l'on veut faire.

Par exemple, avec le bout de code suivant:

```sh
unicode() { sed "s/[‘’]/'/g;s/[“”]/\"/g"; }

mpc="$(mpc --format "%albumartist% ~ %artist% ~ %title%")"
title="$(echo "$mpc" | head -n1 | cut -d'~' -f3 | unicode)"
artist="$(echo "$mpc" | head -n1 | cut -d'~' -f1 | unicode)"
```

Je peux récupérer deux variables `$title` et `$artist` contenant respectivement
le nom et l'artiste de la musique que j'écoute actuellement, pour les afficher
dans ma barre de statut par exemple.

Facile!
Tellement facile qu'à part pour aller sur le net, je peux tout faire dans un
terminal: mails, musique, pdf, code...

### Le code

Étant doctorant en informatique, je passe une partie importante de mon temps à
coder.
J'ai choisi `neovim`[0]: c'est rapide et très extensible, léger et l'édition
modale est extrêmement efficace.

## Ce site

Ce site est hébergé par ARN-FAI[1], une association alsacienne proposant des
services numériques libres et accesibles.
L'association propose notamment la location de serveur privé virtuel (*VPS* en
anglais), me permettant d'héberger ce site.
Je vous renvoie à cette page[2] pour savoir quelles machines sont utilisées.

Pour louer mon nom de domaine, je suis passé par OVH[3].

## Et la suite?

On verra bien.
J'aimerais bien monter une section dédiée aux tutos.
Je me suis rendu compte qu'une large majorité du contenu lié au numérique est
en anglais, et c'est particulièrement vrai en ce qui concerne Linux.
Les *manpages* ne sont pas traduites, ni la doc, ni les *flags*...
Mais puisque je commence à être à l'aise, c'est aussi à mon tour de transmettre
:).
Je vais aussi utiliser ce site pour héberger mes travaux de recherche, et (si
j'y arrive) tenir un blog.
