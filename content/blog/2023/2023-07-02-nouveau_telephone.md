---
title: "Un nouveau téléphone"
description: "Dans ce blog, je parle de mon nouveau téléphone, pourquoi je l'ai
choisi et comment je l'ai configué"
excerpt: "J'ai acheté un téléphone Google. Un Pixel 7 noir 128Go, pour
être précis."
tags:
    - "smartphone"
    - "vie privée"
links:
    - "https://grapheneos.org/"
    - "https://source.android.com/?hl=fr"
    - "https://www.youtube.com/watch?v=Dx7CZ-2Bajg"
    - "https://www.youtube.com/watch?v=l-8qqkCbo3U"
---

## Un QUOI ?!

Eh oui, vous avez bien lu : j'ai acheté un téléphone Google.
Un Pixel 7 noir 128Go, pour être précis.

## Mais pourquoi ? Comment ?

Jusqu'à présent, j'avais un OnePlus 6T dont j'étais plutôt satisfait.
Il tournait sous LineageOS et, bien que je n'avais plus les mises à jour de
sécurité, l'équipe Lineage proposait quand même un maintien de l'OS pour ce
téléphone.

Mais ce téléphone, je l'ai cassé.
Il avait déjà le dos fracturé suite à une chute, mais en septembre dernier j'ai
pris un poteau dans la jambe (!) et la vitre frontale s'est brisée également.
Coup de chance, il était encore utilisable, je n'avais pas de problème d'écran.
Juste... des problèmes d'étanchéité. En effet, les fissures laissent passer la
poussière et les micro-saletés, ce qui a encraissé la caméra frontale.
Pas très grave en principe, mais j'ai commencé à voir des poussières sous la
vitre également, sur les bordures.
En gros, j'allais bientôt avoir des poussières sous l'écran, ce qui aurait
amorcé sa mort.
Et à 300 euros la réparation (puisqu'il fallait, de fait, changer l'écran + la
vitre frontale + la vitre arrière), j'ai préféré faire recycler mon « vieux »
téléphone (près de 4 ans en ma possession, mais téléphone fabriqué en début
d'année 2019) et en racheter un autre.
J'ai longtemps hésité entre les modèles que je pouvais prendre, d'occasion ou
non...
Et j'ai préféré en prendre un neuf, de chez Google.

Un Pixel 7 donc, car (ironiquement) les entreprises qui nous espionnent le plus
sont également celles qui protègent le mieux du *reste du monde*.
Ça vaut pour Google, mais également pour Apple d'ailleurs (qui a été, de
mémoire, le premier fabricant d'ordinateur à chiffrer le disque dur par
défaut).
Les Pixel ont une puce de sécurité dédiée, et globalement bénéficient d'une
certaine approbation sur les composants physiques permettant la sécurité du
terminal.
C'est notamment pour ça que les Pixel sont (quasiment) les seules plateformes
pour lesquelles certaines ROMs Android personnalisées sont disponibles---même
si c'est également dû au fait que Google est assez laxe sur les (dé)blocage du
*bootloader* par exemple, pour encourager et faciliter le développement.

## Et donc ?

Et donc ça permet d'installer une ROM qui propose des possibilités accrues de
gestion de la sécurité mobile : GrapheneOS [0].
Cette ROM est uniquement disponible sur les Pixel et le Fairphone 4.

Le Fairphone 4 propose une alternative très intéressante sur le plan éthique
(même si je ne sais pas où la vérité s'arrête et où le greenwashing commence),
mais aussi et surtout le plan de la réparabilité : leurs produits sont
extrêmement simples à réparer, puisque conçus en ce sens.
Tout ce qu'il vous faut, c'est un tournevis cruciforme.
Tous les composants sont disponibles pour la durée de vie du téléphone, soit 5
ans à partir du début de commercialisation.

Malheureusement, je trouvais le Fairphone bien trop cher pour ce qu'il
proposait.
C'est un téléphone annoncé il y a près de 2 ans, avec des composants de moyenne
gamme pour l'époque, qui souffrait déjà de lenteurs à l'époque.
Pas forcément de sa faute, plutôt celle d'Android et des devs d'application
mobile (et/ou de leur patron.ne :D) qui proposent des applications toujours
plus complexes, donc lourdes.

Un Google Pixel donc.
Que j'ai acheté neuf pour profiter de la reprise de mon ancien téléphone, avec
une réduction bonus octroyée par la boutique, ce qui fait tomber son prix à
490€.
C'est pas donné, mais je bénéficie au moins du support OS jusqu'à octobre 2025,
et des mises à jour de sécurité jusqu'à octobre 2027 !
C'est probablement cela qui fait pencher la balance.
Je reviendrai peut-être un jour sur le besoin croissant que nous avons de nous
protéger des mécanismes de surveillance :).

## GrapheneOS, c'est quoi donc ?

Comme je le disais, GrapheneOS est une ROM personnalisée.
En gros, une équipe de dev modifient le code du projet Android [1] (pas l'OS
tel que proposé par Google, mais bien le projet *open source* de base),
notamment en le nettoyant du code problématique incorporé par Google
(communication permanente avec leurs serveurs, applications intégrées...) et en
rajoutant une couche de sécurité.

Leur travail force le respect, et la ROM propose effectivement des
fonctionnalités intéressantes :
- *sandboxing* des applications Google
- autorisation d'accès au réseau paramétrable pour chaque application
- portée peronnalisable du stockage pour chaque application
- blocage logiciel d'accès à la caméra / micro
- ...

Tout n'est pas parfait, un dev a notamment fait parler de lui récemment [2,3].
Je ne commenterai pas la situation, il a apparemment lâché le développement sur
le projet.
J'espère qu'il s'en remettra.

Ceci étant dit, je suis très satisfait de GrapheneOS.
Ça tourne bien (même si je ne peux pas comparer avec Android stock), la
batterie tient près de deux jours pour mon utilisation (~8h d'écran allumé en
tout), et l'aspect sécurisé est bien fichu.
J'apprécie notamment la gestion du Google Play Store *sandboxé* qui permet
d'avoir tout de même accès aux applications non disponibles sur F-Droid (comme
mon application bancaire), sans pour autant donner trop de libertés au Google
Play Services, responsables de cet espionnage dont je parlais plus tôt.

## Et alors comment t'as fait ?

Ça sera l'occasion d'un autre billet, celui-ci commence déjà à être long !
